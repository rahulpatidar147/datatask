const data = require('./dataSet')
const pro1 = data.map(obj => {
   let str = obj.balance;
   let value = str.split('$');
   let num = value[1].split(',').join('');
   if(isNaN(num)){

      obj['new_balance']= 0
   }else{
      obj['new_balance'] = parseFloat(num);

   }
   return obj;
})
.sort((a,b)=>{

   let aAge = a.age;
   let bAge = b.age;

   return bAge - aAge;
})
.map(obj => {
   
   obj.friends = obj.friends.map((value)=>{

       return Object.values(value)[1]
   })
   return obj;
})
.map(obj => {

   delete obj.tags;
   return obj;

})
.map(obj=>{

   let date = obj.registered.split('-')
   let date_sp = date[2].split('T');
   obj.registered = `${date_sp[0]}/${date[1]}/${date[0]}`
   return obj;
})
.filter(obj=>{

   if(obj.isActive){
       return true;
   }
})
.reduce((pre,curr)=>{

   pre=pre+Number(curr.new_balance);
   return pre
},0)
console.log(pro1);